# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. 使用者可利用`email`進行登入
    2. 將網頁架設在Firebase上
    3. **RWD**設計
    4. 使用者認證後才能寫入Data
    5. 論壇發表文章
    6. 論壇留言
    7. 論壇使用者介面
* Other functions (add/delete)
    1. 使用者可利用`google`進行登入
    2. 有新發文時會有Notification
    3. 在提醒訊息時會有CSS Animation
    4. 使用者可以更新使用者名稱
    5. 使用者可以更改使用者照片
    6. 使用者可以利用信箱找回忘記的密碼
    7. Input的XXS偵測
    8. 頁面邊會隨機顯示照片

### My WebsiteL : https://software-studio-105062308.firebaseapp.com/

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y| UserName 

## Website Detail Description


### Sing In and Sign Up

### 首頁：

一進入論壇之後，會直接進到首頁，右上角會有一個`LogIn`或是`LogOut`  
這個字樣會監聽當前是否有user登入  
如果有User那就會換成LogOut，若沒有就會是LogIn

此外，首頁的左上角有一個**使用者頁面**的連結  
點擊後會載入這個連結的tag也會根據是否`登入`為判斷  

>如果有登入，那此標籤就會顯示User的`DisplayName`  
>如果沒有登入，js就會把這項標籤隱藏起來  

此外，上方的四個頁面標籤，分別是  
***User、Home、Forum、我要發文***  
當點擊該標籤之後，會跳轉到各自的頁面  
跳轉頁面的標籤會以**不同顏色**作為區別

***

### 登入頁面：

點擊LogIn之後，會跳轉到一個新的頁面  
可以輸入使用者的email以及使用者的password  
輸入帳號密碼時會依據使用者輸入的格式是否有誤，給予適當的提醒  
>警告的文字以及標示會出現在網頁的最上方  
並會有`CSS animation`的效果  
提醒顯示4秒鐘之後，就會自動消失

如果沒有註冊帳號，也可以直接點擊`LogIn With Google Account`  
這時會POP出一個Google的登入頁面，隨後就可以直接進入首頁


除此之外，也設計**忘記密碼**的功能  
只要在Email欄位輸入信箱，點選忘記密碼  
系統就會寄信到該使用者的信箱，點選可**重製密碼**

***

### 註冊頁面：

若不想用Google登入，也沒有帳號密碼，就可以點選`Create A new Account`  
這時候會跳轉到一個新的註冊頁面  
輸入使用者名稱、信箱以及密碼就可以註冊成功  
使用者名稱使用firebase中的displayName，並在註冊時同步`update`完成  
同樣的，註冊欄位也會偵測格式錯誤，並且顯示在頁面上方

***

## 文章首頁

### 首頁：

進入首頁之後  
最上方分別會有使用者資料、Home、Froum以及發文的頁面  
在主畫面中，使用了Bootstrap的`container`  
讓使用者即使在手機瀏覽，也可以享有舒適的版面  
首頁中最主要是文章的列表  
>利用firebase的`once`以及`on`函數，可以將存在database的文章列出來  
同時，利用`unshift`，將文章的順序顛倒 
讓最新的文章可以顯示在最上方，也較符合使用者的習慣  

為了不要讓頁面因為文章數不斷增加而越來越長，因此使用滾輪方式  
只顯示特定高度的區塊，文章上會顯示**標題、部分內文以及發布時間、發布者**

除了主要的文章功能外，一旁還有不同的圖片會切換  
將圖片的`url`存入`realtime database`  
載入頁面之後會隨機選取一張照片，因此每次重新整理都會是不同的照片

***
### 文章內容：

在首頁文章列表的每一個文章列表都是一個超連結  
當點擊後就會跳到文章的頁面，在其中會顯示所有文章的內容以及詳細資訊   
此外文章內使用者也可以留言，並且可以即時更新其他人的留言  
和文章列表相同，也是利用firebase的realtime database  
也會把最新發布的留言發佈在最上面  

但是為了維護論壇的發言  
只有**登入後的使用者**才可以發表文章以及留言

***

## RWD

此次的網站是利用bootstrap的Template作為基本模板  
其中利用`container`、`row`、`col`，進行排版  
因此當使用者切換為手機或是平板大小的視窗觀看時  
並不需要額外再放大縮小  

除此之外，**頁面頂部的標籤使用了`nav-bar`**  
當視窗大小縮小時，會變成一個下拉式的選單  
讓整個版面看起來更簡潔

在Desktop版看的時候，除了文章列表，一旁還會有其他訊息  
為了讓手機使用者觀看方便，當版面縮小時會**自動隱藏**一旁的欄位

***

## User Page

User Page會把使用者的資訊顯示在頁面中  
除此之外，也可以點選更改名稱，可以更改User的Display Name  

另外還利用Firebase的`Cloud Storage`，讓使用者可以上傳圖片  
上傳後使用者的大頭貼就可以更新  
若使用者沒有上傳大頭貼，也會有預設的圖片顯示

***

## Chrome Notification
瀏覽器的提醒在新貼文貼出後，會跳出提醒  
點擊Notification後，會跳轉到新貼文的頁面

***

## Security Report (Optional)

### 發文權限

利用firebase的Authentication  
只有在使用者登入的時候才可以發表文章或是留言
但是將`read`恆設為`true`  
因此不論是誰都可以查看文章以及留言

***

### Input 偵測
`<button onclick="alert('haha')" ></button>`  
如果在`input`中打入上述字串  
會使他人可以在留言板中利用script的語法  
更改或是提取database的資料  

為了防止類似的XSS攻擊，利用`string.replace`  
將`<`進行encode成`$lt;`，如此一來在顯示時就不會執行 js 的語法

***
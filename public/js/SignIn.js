const txtEmail = document.getElementById('mail');
const txtPassword = document.getElementById('passwd');
const btnLogin = document.getElementById('btnLogin');
const btnCreate = document.getElementById('btnCreate');
const btnForget = document.getElementById('btnForget');
var box = document.getElementById("custom-alert");


console.log('SignIn included');

var provider = new firebase.auth.GoogleAuthProvider();
var userLogin;

btnLogin.addEventListener('click',function(){

    firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function() {
        window.location.href = "index.html";
    }).catch(function(error){
        var errorCode = error.code;
        var errorMsg = error.message;
        create_alert("error",errorMsg);
    });

},false);

btnLoginGoogle.addEventListener('click',function(){
    firebase.auth().signInWithPopup(provider).then(function(result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        alert(user);
        window.location.href = "index.html";
        }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        create_alert('error',errorMsg);
        });
},false);

btnForgetEmail.addEventListener('click',function(){
    console.log('include');
    firebase.auth().sendPasswordResetEmail(
        txtEmail.value)
        .then(function() {
          // Password reset email sent.
          create_alert('success',"The email has been sent!!!");
        })
        .catch(function(error) {
          var errorMessage = error.message;
          create_alert('error',errorMessage);
        });
},false);


function create_alert(type, message) {
    console.log('include');
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        console.log(message);
        setTimeout("codefans()",5000);
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        console.log(message);
        setTimeout("codefans()",5000);
    }
}

function codefans(){
    box.style.display="none"; 
}
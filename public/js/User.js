var btnLog = document.getElementById('log');
var LogoDisplay = document.getElementById('Logo');
var InputWord = document.getElementById('word');
var box = document.getElementById("custom-alert");
var USemail = document.getElementById('usEmail');
var USName = document.getElementById('usName');
var updateName = document.getElementById('updateName');
var USphoto = document.getElementById('usPhoto');
var fileButton = document.getElementById('fileButton');
var article_id;

function init(){
    firebase.auth().onAuthStateChanged(function(user){
        //var user = firebase.auth().currentUser;    
        console.log('forum.js included');
        //console.log(user);
        if(user){
            var user_mail = user.email;
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log out</a>';
            LogoDisplay.innerHTML = '<a class="navbar-brand" href="User.html"><img id="icon" src="' 
            + user.photoURL + '"> '+ user.displayName +'</a>';

            USemail.innerHTML = "<h2>Email: </h2><p style='word-break: break-all;'>" + user.email + "</p>";
            USName.innerHTML = "<h2>Name: </h2><p style='word-break: break-all;'>" + user.displayName + "</p>";
            if(user.photoURL){    
                USphoto.innerHTML = "<a><img id='USPhoto' src='" + user.photoURL + "'></a>";
            }
            console.log(user.photoURL);
        }
        else{
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log In</a>';
            LogoDisplay.classList.add('hide');
        }
    });

    fileButton.addEventListener('change',function(e){
        var user = firebase.auth().currentUser;
        var file = e.target.files[0];

        var storageRef = firebase.storage().ref('Photo/' + user.email);
        storageRef.put(file).then(function(url){
            console.log(url.metadata.downloadURLs);
            user.updateProfile({
                photoURL:url.metadata.downloadURLs
            });
            window.setTimeout("doUpdate()", 1500);
        });

    });

}


function updateFirebase(){
    var user = firebase.auth().currentUser;
    if(updateName.value==""){
        create_alert("error","Your Name Can't be Empty!!");
        console.log('Error');
    }else{    
        user.updateProfile({
        displayName : updateName.value,});
        //Check whether the user finished all the column
        window.setTimeout("doUpdate()", 1000);
    }
}

function LogControl(){
    var user = firebase.auth().currentUser;    
    if(user){
        firebase.auth().signOut().then(function(){
            //create_alert('success','You have Sign out Successfully!');
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log In</a>';
            window.location.href = "index.html";
        }).catch(function (error){
            var errorCode = error.code;
            var errorMsg = error.message;
            create_alert("error",errorMsg);
        });
        
    }
    else{
        window.location.href = "SignIn.html";
    }
};

function errData(){

}


window.onload = function () {
    init();
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alt diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        setTimeout("codefans()",4000);
    } else if (type == "error") {
        str_html = "<div class='alt diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        setTimeout("codefans()",4000);
    }
}

function codefans(){
    box.style.display="none"; 
}

function doUpdate(){
    window.location.href = "User.html";
}
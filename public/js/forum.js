var btnLog = document.getElementById('log');
var LogoDisplay = document.getElementById('Logo');
var InputWord = document.getElementById('word');
var box = document.getElementById("custom-alert");
var article_id;

function init(){
    firebase.auth().onAuthStateChanged(function(user){
        //var user = firebase.auth().currentUser;    
        console.log('forum.js included');
        //console.log(user);
        if(user){
            var user_mail = user.email;
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log out</a>';
            LogoDisplay.innerHTML = '<a class="navbar-brand" href="User.html"><img id="icon" src="' 
            + user.photoURL + '"> '+ user.displayName +'</a>';

        }
        else{
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log In</a>';
            LogoDisplay.classList.add('hide');
        }
    })

    var url = location.href.split('?');
    article_id = url[1];

    var ref = firebase.database().ref('users/' + article_id);
    ref.on('value',gotData,errData);

    var imgref = firebase.database().ref('words/' + Math.floor((Math.random() * 7) + 1));
    imgref.on('value',function(snapshot){
        document.getElementById("inspire").innerHTML = 
        '<img id="img" src="' + 
        snapshot.val() + 
        '"></div>';
        console.log(snapshot.val());
    });


    //load comment
    var str_before_username = "<div class='col-sm-8 post'>";
    var str_after_content = "</div></a></div></div>\n";

    var postsRef = firebase.database().ref('users/' + article_id + '/comment');
    var artRef = firebase.database().ref('users/');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {

            snapshot.forEach(function (childSnapshot) {
                var user = firebase.auth().currentUser;
                var childData = childSnapshot.val();
                
                total_post.unshift(str_before_username + 
                "<div class='panel panel-success'><div class='panel-heading'><a><strong class='d-block text-gray-dark'>" + 
                '留言的人 : ' + childData.userName + '</div><div class="panel-body">' + childData.comment + 
                '<p id = "time">Time:  ' + childData.Time +'</p>' + "</strong>" 
                + str_after_content);
                first_count += 1;
            });
            document.getElementById("cmtList").innerHTML = total_post.join("");
            postsRef.on("child_added",function(data){
                var user = firebase.auth().currentUser;
                second_count+=1;
                var childData = data.val();
                if(second_count>first_count){
                    total_post.unshift(str_before_username + 
                        "<div class='panel panel-success'><div class='panel-heading'><a><strong class='d-block text-gray-dark'>" + 
                        '留言的人 : ' + childData.userName + '</div><div class="panel-body">' + childData.comment + 
                        '<p id = "time">Time:  ' + childData.Time +'</p>' + "</strong>" 
                        + str_after_content);
                    document.getElementById("cmtList").innerHTML = total_post.join("");
                }
            });
        
        })
        .catch(e => console.log(e.message));

    var first_ct = 0;
    // Counter for checking when to update new post
    var second_ct = 0;

    artRef.once('value')
    .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            first_ct += 1;
            console.log('hehe');
        });
        artRef.on("child_added",function(data){
            second_ct+=1;
            if(second_ct>first_ct){
                console.log('here');
                if(Notification){
                    var notification = new Notification('Notification title', {
                        icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                        body: "Some Has Created A New Post!!!",
                        });
                    notification.onclick = function () {
                        window.open("forum.html?" + data.key);      
                    };
                }
            }
        });
    }).catch(e => console.log(e.message));

}


function LogControl(){
    
    var user = firebase.auth().currentUser;    
    if(user){
        firebase.auth().signOut().then(function(){
            create_alert('success','You have Sign out Successfully!');
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log In</a>';
            window.location.href = "index.html";
        }).catch(function (error){
            var errorCode = error.code;
            var errorMsg = error.message;
            create_alert("error",errorMsg);
        });
    }
    else{
        window.location.href = "SignIn.html";
    }
};


function updateFirebase(){
    const fb = firebase.database().ref();
    //Check whether the user finished all the column

    var cmt = $("#comment").val();
    if(cmt==""){
        create_alert('error','You Haven\'t Type Anything Yet!!!');
        return;
    }

    var res = cmt.replace(/</g, "&lt;");


    var user = firebase.auth().currentUser;
    if(user){
        var dt = new Date();
        var date = dt.getFullYear() + '/' + dt.getMonth() + '/' + dt.getDate() + "  " + dt.getHours() + ":" + dt.getMinutes();    

        data = {
            userName: user.displayName,
            comment: res,
            Time:date
        }

        fb.child( 'users/' + article_id + '/comment').push(data);
        document.getElementById('comment').value = "";
    }
    else{
        create_alert('error',"You Have To Sign In First!!");
    }

}

function gotData(data){
    var article = data.val();
    var article_str ="<h3 class='head'>標題：" + article.title + "</h3>";
    var article_id = "<h3 class='head'>作者 : " + article.userName + "</h3>"
    var article_time = "<h3 class='head'>時間 : " + article.Time + "</h3>";
    var article_cnt ='<pre style="font-size:20px; font-family:Microsoft JhengHei;">' +article.content + '</pre>'
    document.getElementById('display').innerHTML = article_str + article_id + article_time + article_cnt;
}

function errData(){

}


window.onload = function () {
    init();
}


function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alt diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        setTimeout("codefans()",4000);
    } else if (type == "error") {
        str_html = "<div class='alt diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        setTimeout("codefans()",4000);
    }
}

function codefans(){
    box.style.display="none"; 
}
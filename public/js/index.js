var btnLog = document.getElementById('log');
var LogoDisplay = document.getElementById('Logo');
var InputWord = document.getElementById('word');
var box = document.getElementById("custom-alert");
var article_id;
var btn = document.getElementById('btn');

function init(){
    firebase.auth().onAuthStateChanged(function(user){
        //var user = firebase.auth().currentUser;    
        console.log('index.js included');
        //console.log(user);
        if(user){
            var user_mail = user.email;
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log out</a>';
            LogoDisplay.innerHTML = '<a class="navbar-brand" href="User.html"><img id="icon" src="' 
            + user.photoURL + '"> '+ user.displayName +'</a>';

        }
        else{
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log In</a>';
            LogoDisplay.classList.add('hide');
        }
    })

    var url = location.href.split('?');
    article_id = url[1];

    var ref = firebase.database().ref('users/' + article_id);

    var imgref = firebase.database().ref('words/' + Math.floor((Math.random() * 7) + 1));
    imgref.on('value',function(snapshot){
        document.getElementById("inspire").innerHTML = 
        '<img id="img" src="' + 
        snapshot.val() + 
        '"></div>';
        console.log(snapshot.val());
    });
    //ref.on('value',gotData,errData);


    // The html code for post
    var str_before_username = "<div class = 'panel panel-info'><div class='panel-heading post'><div class='media text-muted pt-3'>"
     ;
    var str_after_content = "</div></a></div></div>\n";

    var postsRef = firebase.database().ref('users');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {

            snapshot.forEach(function (childSnapshot) {
                var user = firebase.auth().currentUser;
                var childData = childSnapshot.val();
                //total_post[total_post.length] = str_before_username + "<a href='forum.html?' id ='" + childSnapshot.key + "' onclick = 'turn(this.id)'>" + "><strong class='d-block text-gray-dark'>" + childData.userName + "</strong>" + childData.title + str_after_content;
                total_post.unshift(str_before_username + "<a href='forum.html?" + 
                childSnapshot.key + "' ><strong class='d-block text-gray-dark'>" +
                 "[爆卦]" + childData.title + '</div></div><div class="panel-body">'+childData.content.substring(0,5) +'........<br>'+ "</strong>"+ 
                 childData.userName + '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Time:  ' 
                 + childData.Time  
                 + str_after_content);
                first_count += 1;
            });
            document.getElementById("artList").innerHTML = total_post.join("");
            postsRef.on("child_added",function(data){
                var user = firebase.auth().currentUser;
                second_count+=1;
                var childData = data.val();
                if(second_count>first_count){
                    total_post.unshift(str_before_username + "<a href='forum.html?" +
                    data.key + "' ><strong class='d-block text-gray-dark'>" +
                    "[爆卦]" +  childData.title + '</div></div><div class="panel-body">' + childData.content.substring(0,5) +'........<br>' + "</strong>" 
                    + childData.userName + '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Time:  ' 
                    + childData.Time
                    + str_after_content);
                    document.getElementById("artList").innerHTML = total_post.join("");

                    if(Notification){
                        var notification = new Notification('Notification title', {
                            icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                            body: "Some Has Created A New Post!!!",
                          });
                        notification.onclick = function () {
                            window.open("forum.html?" + data.key);      
                        };
                    }
                }
            });
        
        })
        .catch(e => console.log(e.message));

        
        if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.'); 
            return;
        }else if (Notification.permission !== "granted"){
            Notification.requestPermission();
        }


}

function LogControl(){
    
    var user = firebase.auth().currentUser;    
    if(user){
        firebase.auth().signOut().then(function(){
            //create_alert('success','You have Sign out Successfully!');
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log In</a>';
            setTimeout(window.location.href = "index.html",2000);
        }).catch(function (error){
            var errorCode = error.code;
            var errorMsg = error.message;
            create_alert("error",errorMsg);
        });
        
    }
    else{
        window.location.href = "SignIn.html";
    }
};

function gotData(data){

    console.log(data.val());
    var article = data.val();
    var article_str = "<h1>" + article.title + "</h1>";
    var article_id = "<h1>" + article.userName + "</h1>"
    document.getElementById('display').innerHTML = article_str + "<br>" + article_id;
}

function errData(){
}

window.onload = function () {
    init();
}


function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alt diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        setTimeout("codefans()",4000);
    } else if (type == "error") {
        str_html = "<div class='alt diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        setTimeout("codefans()",4000);
    }
}

function codefans(){
    box.style.display="none"; 
}

var btnLog = document.getElementById('log');
var LogoDisplay = document.getElementById('Logo');
var InputWord = document.getElementById('word');
var box = document.getElementById("custom-alert");
var article_id;
var New_Post_id;


function init(){
    firebase.auth().onAuthStateChanged(function(user){
        //var user = firebase.auth().currentUser;    
        console.log('forum.js included');
        //console.log(user);
        if(user){
            var user_mail = user.email;
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log out</a>';
            LogoDisplay.innerHTML = '<a class="navbar-brand" href="User.html"><img id="icon" src="' 
            + user.photoURL + '"> '+ user.displayName +'</a>';

        }
        else{
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log In</a>';
            LogoDisplay.classList.add('hide');
        }
    })

    var postsRef = firebase.database().ref('users');
    postsRef.on("child_added",function(data){
        New_Post_id = data.key;
    });

    var url = location.href.split('?');
    article_id = url[1];
    console.log(article_id);

}

function LogControl(){
    
    var user = firebase.auth().currentUser;    
    if(user){
        firebase.auth().signOut().then(function(){
            create_alert('success','You have Sign out Successfully!');
            btnLog.innerHTML = '<a  href="#" onclick="LogControl()"><span class="glyphicon glyphicon-log-in"></span> Log In</a>';
            window.location.href = "index.html";
        }).catch(function (error){
            var errorCode = error.code;
            var errorMsg = error.message;
            create_alert("error",errorMsg);
        });
        
    }
    else{
        window.location.href = "SignIn.html";
    }
};

function updateFirebase(){
    const fb = firebase.database().ref();

    
    var ctx = $("#content").val();
    var title = InputWord.value;
    if(title=="" || ctx==""){
        create_alert('error','You haven\'t finished yet!!!');
        return;
    }

    var res = ctx.replace(/</g, "&lt;");
    var til = title.replace(/</g, "&lt;");

    var user = firebase.auth().currentUser;
    if(user){
        var dt = new Date();
        var date = dt.getFullYear() + '/' + (dt.getMonth()+1) + '/' + dt.getDate() + "  " + dt.getHours() + ":" + dt.getMinutes();       
        data = {
            userName: user.displayName,
            title: til,
            Time:date,
            content:res,
            comment:""
        }

        fb.child('users').push(data);
        InputWord.value = "";
        
        window.location.href = "forum.html?" + New_Post_id;
    }
    else{
        create_alert('error','You Have To Sing In First!!');
    }

}

window.onload = function () {
    init();
}


function create_alert(type, message) {
    console.log('include');
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alt diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        console.log(message);
        setTimeout("codefans()",4000);
    } else if (type == "error") {
        str_html = "<div class='alt diss' role='alert'><strong>Error! </strong>" + message + "<span aria-hidden='true'></span></div>";    
        box.style.display="block"; 
        alertarea.innerHTML = str_html;
        console.log(message);
        setTimeout("codefans()",4000);
    }
}

function codefans(){
    box.style.display="none"; 
}